package org.mcslab.beaconscanner;

import android.content.Context;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "BeaconScanner" ;

    Button btStart = null ;
    Button btStop = null ;
    Button btClean = null ;

    BeaconManager beaconManager = null ;

    long mCurrentTimeMillis = 0 ;


    private static final UUID ESTIMOTE_PROXIMITY_UUID = UUID.fromString("00000000-0000-0000-C000-000000000028");
    private static final Region ALL_ESTIMOTE_BEACONS = new Region("rid", ESTIMOTE_PROXIMITY_UUID, null, null);

    SimpleDateFormat nowdate = new java.text.SimpleDateFormat("HH:mm:ss:SSS");

    String sdate = null ;

    File file = null ;
    private final static String filename = "beaconsanner-" ;

    FileOutputStream outputStream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Region ALL_ESTIMOTE_BEACONS = new Region("regionID", ESTIMOTE_PROXIMITY_UUID,null,null);




        nowdate.setTimeZone(TimeZone.getTimeZone("GMT+8"));

        btStart = (Button) findViewById(R.id.bt_start);
        btStop = (Button) findViewById(R.id.bt_stop) ;
        btClean = (Button) findViewById(R.id.bt_clean) ;





        beaconManager = new BeaconManager(getApplicationContext());

        // add this below:
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.setBackgroundScanPeriod(200, 0);
                //btStart.setEnabled(true);
                Toast.makeText(getApplicationContext(),"beacon manager OK", Toast.LENGTH_SHORT).show();

            }
        });

        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> list) {



                mCurrentTimeMillis = System.currentTimeMillis() ;

                sdate = nowdate.format(new java.util.Date());



                for(Beacon mBeacon : list){
                    //Log.i(TAG,mBeacon.toString());
                    Log.i(TAG, sdate + " "+mCurrentTimeMillis + " " + mBeacon.getMajor() + ":"+mBeacon.getMinor() + " Rssi = " + mBeacon.getRssi() ) ;

                    //outputStream.write(sdate + " "+mCurrentTimeMillis + " " + mBeacon.getMajor() + ":"+mBeacon.getMinor() + " Rssi = " + mBeacon.getRssi());

                    try{
                        outputStream.write(sdate.getBytes());
                        outputStream.write(" ".getBytes());
                        outputStream.write(Long.toString(mCurrentTimeMillis).getBytes());
                        outputStream.write(" ".getBytes());
                        outputStream.write(Integer.toString(mBeacon.getMajor()).getBytes());
                        outputStream.write(":".getBytes());
                        outputStream.write(Integer.toString(mBeacon.getMinor()).getBytes());
                        outputStream.write(":".getBytes());
                        outputStream.write(" Rssi : ".getBytes());
                        outputStream.write(Integer.toString(mBeacon.getRssi()).getBytes());
                        outputStream.write("\n".getBytes());

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }

                Toast.makeText(getApplicationContext(),getString(R.string.there_are_beacons,list.size()),Toast.LENGTH_SHORT).show();


            }
        });


        btStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                file = new File(Environment.getExternalStorageDirectory() + "/"+filename + System.currentTimeMillis()+".txt");

                Log.i(TAG,file.getAbsolutePath());

                if(!file.exists()){
                    try {
                        file.createNewFile() ;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    outputStream = new FileOutputStream(file);

                } catch (Exception e) {
                    e.printStackTrace();
                }



                beaconManager.startRanging(ALL_ESTIMOTE_BEACONS);
                Toast.makeText(getApplicationContext(),getString(R.string.start),Toast.LENGTH_SHORT).show();



            }
        });

        btStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS);
                Toast.makeText(getApplicationContext(),getString(R.string.stop),Toast.LENGTH_LONG).show();

                try {
                    outputStream.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        btClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();


        beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS);
        Toast.makeText(getApplicationContext(),getString(R.string.stop),Toast.LENGTH_LONG).show();

        try {
            outputStream.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
